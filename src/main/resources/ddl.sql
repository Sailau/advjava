
CREATE TABLE public.agroup
(
    id integer NOT NULL,
    name character varying(10) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT agroup_pkey PRIMARY KEY (id)
)

CREATE TABLE public.students
(
    id integer NOT NULL,
    name character varying(10) COLLATE pg_catalog."default" NOT NULL,
    phone character varying(12) COLLATE pg_catalog."default" NOT NULL,
    groupid integer NOT NULL,
    CONSTRAINT students_pkey PRIMARY KEY (id),
    CONSTRAINT fk_gid FOREIGN KEY (groupid)
        REFERENCES public.agroup (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
