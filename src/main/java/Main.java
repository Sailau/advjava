import entity.Group;
import entity.Students;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        Group a = new Group();
        Students b = new Students();
        System.out.println(a.toString());
        System.out.println(b.toString());
        Connection connection = null;

        try {

            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/daulet", "postgres", "9December!");

            if (connection != null) {
                System.out.println("Connection successful!");
            } else {
                System.out.println("Please, recheck your Login or Password");
            }

            Statement statement = null;
            statement = connection.createStatement();
            ResultSet result1 = statement.executeQuery(
                    "SELECT * from agroup"
            );
            ResultSet result2 = statement.executeQuery(
                    "SELECT *from Students"
            );

            while (result1.next()) {
                System.out.println("Айди " + result1.getInt("id") + " Name of group:" + result1.getString("name"));
            }

            while (result2.next()) {
                System.out.println("Айди " + result2.getInt("id") + " Name of Student: " + result2.getString("name") + " Phone Number " + result2.getString("phone") + " GroupID " + result2.getString("groupId"));
            }
            statement.close();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (connection != null) {
                connection.close();
            }

     /*   Scanner console = new Scanner(System.in);
        //String a = console.nextLine();
        int a = console.nextInt();
        int b= console.nextInt();
            new Check(a,b);*/

        }


    }
}
